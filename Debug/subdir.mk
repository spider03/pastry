################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Execution.cpp \
../Helper.cpp \
../MD5.cpp \
../Main.cpp \
../ServerRequest.cpp \
../ServerUtility.cpp \
../Utility.cpp 

OBJS += \
./Execution.o \
./Helper.o \
./MD5.o \
./Main.o \
./ServerRequest.o \
./ServerUtility.o \
./Utility.o 

CPP_DEPS += \
./Execution.d \
./Helper.d \
./MD5.d \
./Main.d \
./ServerRequest.d \
./ServerUtility.d \
./Utility.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -pthread -std=c++11 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


